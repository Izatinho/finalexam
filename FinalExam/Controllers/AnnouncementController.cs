﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.DAL.Entities;
using FinalExam.Models;
using FinalExam.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FinalExam.Controllers
{
    public class AnnouncementController : Controller
    {
        private readonly IAnnouncementService _announcementService;
        private readonly UserManager<User> _userManager;

        public AnnouncementController(IAnnouncementService announcementService, UserManager<User> userManager)
        {
            _announcementService = announcementService;
            _userManager = userManager;
        }

        public IActionResult Index(AnnouncementIndexModel model)
        {
            try
            {
                var announceModels = _announcementService.GetAllAnnouncements(model);
                model.Announcements = announceModels;
                return View(model);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
            
        }

        [Authorize]
        public IActionResult Create()
        {
            var model = _announcementService.GetAllAnnouncementCreateModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateAnnouncement(AnnouncementCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _announcementService.CreateAnnouncement(model, currentUser.Id);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Details(int announcementId)
        {
            var announceModel = _announcementService.GetAnnouncementById(announcementId);

            return View(announceModel);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Like(int announcementId)
        {
            _announcementService.AddLike(announcementId);

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult LikeAjax(int announcementId)
        {
            try
            {
                var likes = _announcementService.AddLike(announcementId);

                return Ok(likes);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult UpAnnouncement(int announcementId)
        {
            try
            {
                var date = _announcementService.UpAnnouncementDate(announcementId);
                return Ok(date);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddComment(AddCommentRequestModel model)
        {
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                var commentModel = _announcementService.AddComment(model, currentUser);

                return Ok(commentModel);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult Upload(AddGalleryImageModel model)
        {
            try
            {
                _announcementService.UploadImages(model);
                return RedirectToAction("Details", new { announcementId = model.AnnouncementId });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult UploadAjax(int announcementId)
        {
            try
            {
                AddGalleryImageModel model = new AddGalleryImageModel()
                {
                    AnnouncementId = announcementId,
                    Images = Request.Form.Files
                };

                var imageModels = _announcementService.UploadImages(model);

                return Ok(imageModels.ToList());
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
