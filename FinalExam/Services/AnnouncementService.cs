﻿using AutoMapper;
using FinalExam.DAL.Entities;
using FinalExam.DAL.Repositories.Contracts;
using FinalExam.Models;
using FinalExam.Services.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FinalExam.Services
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;
        private readonly DbFilesSaver _dbFileSaver;

        public AnnouncementService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver, DbFilesSaver dbFileSaver)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
            _dbFileSaver = dbFileSaver;
        }

        public AnnouncementCreateModel GetAllAnnouncementCreateModel()
        {
            return new AnnouncementCreateModel()
            {
                CategoriesSelect = GetCategoriesSelect()
            };
        }
        public SelectList GetCategoriesSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                return new SelectList(categories, nameof(Category.Id), nameof(Category.Name));
            }
        }

        public List<AnnouncementModel> GetAllAnnouncements(AnnouncementIndexModel model)
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var announcements = uow.Announcements.GetAllWithAuthorsAndComments();

                announcements = announcements
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.DateFrom)
                    .ByDateTo(model.DateTo);

                int pageSize = 4;
                int count = announcements.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                announcements = announcements.Skip((page - 1) * pageSize).Take(pageSize);
                model.PagingModel = new PagingModel(count, page, pageSize);
                model.Page = page;

                var models = Mapper.Map<List<AnnouncementModel>>(announcements.ToList());

                models.OrderBy(p=>p.DatePublished);
                return models;


            }
        }

        public void CreateAnnouncement(AnnouncementCreateModel model, int currentUserId)
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var announcement = Mapper.Map<Announcement>(model);
                announcement.AuthorId = currentUserId;

                _fileSaver.SaveFile(announcement, model.Image);

                uow.Announcements.Create(announcement);
            }
        }

        public AnnouncementModel GetAnnouncementById(in int announcementId)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var announcement = uow.Announcements.GetByIdWithAuthorsAndComments(announcementId);
                return Mapper.Map<AnnouncementModel>(announcement);
            }
        }

        public int AddLike(int anouncementId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var announcement = unitOfWork.Announcements.GetById(anouncementId);
                announcement.Likes++;
                unitOfWork.Announcements.Update(announcement);
                return announcement.Likes;
            }
        }
        public DateTime UpAnnouncementDate(int announcementId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var announcement = unitOfWork.Announcements.GetById(announcementId);
                announcement.DatePublished = DateTime.Now;
                unitOfWork.Announcements.Update(announcement);

                return announcement.DatePublished;
            }
        }

        public CommentModel AddComment(AddCommentRequestModel model, User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var comment = Mapper.Map<Comment>(model);
                comment.AuthorId = currentUser.Id;
                comment.CreatedOn = DateTime.Now;
                unitOfWork.Comments.CreateAsync(comment);
                comment.Author = currentUser;
                return Mapper.Map<CommentModel>(comment);
            }
        }

        public IEnumerable<GalleryImageModel> UploadImages(AddGalleryImageModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                foreach (var image in model.Images)
                {
                    var galleryImage = new GalleryImage()
                    {
                        AnnouncementId = model.AnnouncementId,
                        Name = image.FileName,
                        Image = _dbFileSaver.GetImageBytes(image)
                    };
                    unitOfWork.GalleryImages.Create(galleryImage);
                    yield return new GalleryImageModel()
                    {
                        Image = galleryImage.Image,
                        Name = galleryImage.Name
                    };
                }
            }
        }

        
    }
}
