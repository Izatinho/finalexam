﻿using FinalExam.DAL.Entities;
using FinalExam.Services.Contracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Services
{
    public class DbFilesSaver : IFileSaver
    {
        public void SaveFile(Announcement announcement, IFormFile formFile)
        {
            announcement.Image = GetImageBytes(formFile);
            announcement.AnnouncementImageType = AnnouncementImageType.Db;
        }

        public byte[] GetImageBytes(IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                return binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}
