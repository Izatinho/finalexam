﻿using FinalExam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Services
{
    public static class AnnouncementServiceExtensions
    {
        public static IEnumerable<Announcement> BySearchKey(this IEnumerable<Announcement> announcements, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                announcements = announcements.Where(r => r.Title.Contains(searchKey));

            return announcements;
        }
        public static IEnumerable<Announcement> ByAuthorName(this IEnumerable<Announcement> announcements, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                announcements = announcements.Where(r => r.Author.UserName.Contains(authorName));

            return announcements;
        }

        public static IEnumerable<Announcement> ByDateFrom(this IEnumerable<Announcement> announcements, DateTime? dateFrom)
        {
            if (dateFrom.HasValue)
                announcements = announcements.Where(r => r.DatePublished >= dateFrom.Value);

            return announcements;
        }
        public static IEnumerable<Announcement> ByDateTo(this IEnumerable<Announcement> announcements, DateTime? dateTo)
        {
            if (dateTo.HasValue)
                announcements = announcements.Where(r => r.DatePublished <= dateTo.Value);

            return announcements;
        }

    }
}
