﻿using FinalExam.DAL.Entities;
using Microsoft.AspNetCore.Http;


namespace FinalExam.Services.Contracts
{
    public interface IFileSaver
    {
        void SaveFile(Announcement announcement, IFormFile formFile);
    }
}
