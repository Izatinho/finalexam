﻿using FinalExam.DAL.Entities;
using FinalExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Services.Contracts
{
    public interface IAnnouncementService
    {
        List<AnnouncementModel> GetAllAnnouncements(AnnouncementIndexModel model);
        AnnouncementCreateModel GetAllAnnouncementCreateModel();
        void CreateAnnouncement(AnnouncementCreateModel model, int currentUserId);
        AnnouncementModel GetAnnouncementById(in int announcementId);
        int AddLike(int anouncementId);
        CommentModel AddComment(AddCommentRequestModel model, User currentUser);
        IEnumerable<GalleryImageModel> UploadImages(AddGalleryImageModel model);

        DateTime UpAnnouncementDate(int announcementId);


    }
}
