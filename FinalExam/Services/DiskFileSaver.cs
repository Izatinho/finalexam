﻿using FinalExam.DAL.Entities;
using FinalExam.Services.Contracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace FinalExam.Services
{
    public class DiskFileSaver : IFileSaver
    {
        private readonly IWebHostEnvironment _hostEnvironment;

        public DiskFileSaver(IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }

        public void SaveFile(Announcement announcement, IFormFile formFile)
        {
            var fileFullName = formFile.FileName;
            var fileId = Guid.NewGuid();
            var fileName = Path.GetFileNameWithoutExtension(fileFullName);
            var fileExtension = Path.GetExtension(fileFullName);

            string filePath = $"/files/{fileName}-{fileId}{fileExtension}";
            announcement.ImagePath = filePath;
            announcement.AnnouncementImageType = AnnouncementImageType.Disk;
            using (var fileStream = new FileStream(_hostEnvironment.WebRootPath + filePath, FileMode.Create))
            {
                formFile.CopyTo(fileStream);
            }
        }
    }
}
