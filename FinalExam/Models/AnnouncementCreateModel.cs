﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class AnnouncementCreateModel
    {
        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Категория должно быть указана")]
        public int CategoryId { get; set; }
        
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Телефон продавца")]
        public string UserPhone { get; set; }
        [Required]
        [Display(Name = "Изображение")]
        public IFormFile Image { get; set; }
        public SelectList CategoriesSelect { get; set; }
    }
}
