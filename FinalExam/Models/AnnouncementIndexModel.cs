﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class AnnouncementIndexModel
    {
        public string Author { get; set; }
        public string SearchKey { get; set; }
        public string Category { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public List<AnnouncementModel> Announcements { get; set; }
        public int? Page { get; set; }
        public PagingModel PagingModel { get; set; }
    }
}
