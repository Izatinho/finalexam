﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace FinalExam.Models
{
    public class AddGalleryImageModel
    {
        [Required]
        public int AnnouncementId { get; set; }

        public IFormFileCollection Images { get; set; }
    }
}
