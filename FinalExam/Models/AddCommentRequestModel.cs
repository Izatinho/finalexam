﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class AddCommentRequestModel
    {
        [Required]
        [JsonProperty(propertyName: "comment")]
        public string Comment { get; set; }

        [Required]
        [JsonProperty(propertyName: "announcementId")]
        public int AnnouncementId { get; set; }
    }
}
