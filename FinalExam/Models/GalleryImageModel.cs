﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class GalleryImageModel
    {
        public string Name { get; set; }
        public byte[] Image { get; set; }
    }
}
