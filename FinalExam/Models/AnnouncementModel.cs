﻿using FinalExam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class AnnouncementModel
    {
        public int Id { get; set; }

        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Display(Name = "Дата публикации")]
        public string DatePublished { get; set; }
        [Display(Name = "Автор")]
        public string AuthorName { get; set; }
        [Display(Name = "Категория")]
        public string CategoryName { get; set; }
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
        [Display(Name = "Телефон Автора")]
        public string UserPhone { get; set; }
        [Display(Name = "Лайки")]
        public int Likes { get; set; }
        public byte[] Image { get; set; }
        public string ImagePath { get; set; }
        public AnnouncementImageType AnnouncementImageType { get; set; }
        public List<CommentModel> Comments { get; set; }
        public List<GalleryImageModel> Images { get; set; }
    }
}
