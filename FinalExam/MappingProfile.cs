﻿using AutoMapper;
using FinalExam.DAL.Entities;
using FinalExam.Models;
using System;

namespace FinalExam
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateAnnounceCreateModelToAnnouncement();
            CreateAnnouncementToAnnouncementModelMap();
            CreateAddCommentRequestToCommentMap();
            CreateCommentToCommentModelMap();
        }

        public void CreateAnnounceCreateModelToAnnouncement()
        {
            CreateMap<AnnouncementCreateModel, Announcement>()
                .ForMember(target => target.DatePublished,
                src => src.MapFrom(p => DateTime.Now))
                .ForMember(target => target.Image,
                src => src.Ignore());
        }

        public void CreateAnnouncementToAnnouncementModelMap()
        {
            CreateMap<Announcement, AnnouncementModel>()
                .ForMember(target => target.DatePublished,
                src => src.MapFrom(p => p.DatePublished.ToString("D")))
                .ForMember(target => target.AuthorName,
                    src => src.MapFrom(p => p.Author.UserName))
                .ForMember(target => target.Images,
                src => src.MapFrom(p => p.GalleryImages));
        }
        private void CreateCommentToCommentModelMap()
        {
            CreateMap<Comment, CommentModel>()
                .ForMember(target => target.CreatedOn,
                    src => src.MapFrom(p => p.CreatedOn.ToString("D")))
                .ForMember(target => target.AuthorName,
                    src => src.MapFrom(p => p.Author.UserName));
        }

        private void CreateAddCommentRequestToCommentMap()
        {
            CreateMap<AddCommentRequestModel, Comment>()
                .ForMember(target => target.Content,
                    src => src.MapFrom(p => p.Comment));
        }
    }
}