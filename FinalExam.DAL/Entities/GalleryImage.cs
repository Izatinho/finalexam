﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Entities
{
    public class GalleryImage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public int AnnouncementId { get; set; }
        public Announcement Announcement { get; set; }

    }
}
