﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Entities
{
    public class Comment : IEntity
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int AnnouncementId { get; set; }
        public Announcement Announcement { get; set; }
    }
}
