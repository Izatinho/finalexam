﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace FinalExam.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Announcement> Announcements { get; set; }    
    }
}
