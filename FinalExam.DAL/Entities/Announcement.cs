﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Entities
{
    public class Announcement : IEntity
    {
        public int Id { get; set; }
        public DateTime DatePublished { get; set; }
        public string Title { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public decimal Price { get; set; }
        public int Likes { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public string ImagePath { get; set; }
        public string UserPhone { get; set; }

        public byte[] Image { get; set; }

        public AnnouncementImageType AnnouncementImageType { get; set; }
        public ICollection<Comment> Comments { get; set; }

        public ICollection<GalleryImage> GalleryImages { get; set; }
    }
}
