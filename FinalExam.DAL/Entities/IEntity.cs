﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
