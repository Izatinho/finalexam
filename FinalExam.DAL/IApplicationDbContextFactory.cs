﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
