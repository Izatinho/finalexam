﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FinalExam.DAL.Migrations
{
    public partial class AddImagePathToAnnouncement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "Announcements",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Announcements",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RecordImageType",
                table: "Announcements",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Announcements");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Announcements");

            migrationBuilder.DropColumn(
                name: "RecordImageType",
                table: "Announcements");
        }
    }
}
