﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FinalExam.DAL.Migrations
{
    public partial class addingLikesToAnnouncementEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Likes",
                table: "Announcements",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Likes",
                table: "Announcements");
        }
    }
}
