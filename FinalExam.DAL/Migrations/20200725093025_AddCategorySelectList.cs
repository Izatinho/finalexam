﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FinalExam.DAL.Migrations
{
    public partial class AddCategorySelectList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CategoryId",
                table: "Announcements",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CategoryId",
                table: "Announcements",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
