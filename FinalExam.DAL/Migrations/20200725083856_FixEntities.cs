﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FinalExam.DAL.Migrations
{
    public partial class FixEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecordImageType",
                table: "Announcements");

            migrationBuilder.AddColumn<int>(
                name: "AnnouncementImageType",
                table: "Announcements",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UserPhone",
                table: "Announcements",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnnouncementImageType",
                table: "Announcements");

            migrationBuilder.DropColumn(
                name: "UserPhone",
                table: "Announcements");

            migrationBuilder.AddColumn<int>(
                name: "RecordImageType",
                table: "Announcements",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
