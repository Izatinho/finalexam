﻿using FinalExam.DAL.Entities;
using FinalExam.DAL.EntityConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FinalExam.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<GalleryImage> GalleryImages { get; set; }
        public DbSet<Category> Categories { get; internal set; }

        public ApplicationDbContext(
             DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.AnnouncementConfiguration.
                ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.CommentConfiguration.
                ProvideConfigurationAction());

            DisableOneToManyCascadeDelete(builder);
        }

        private void DisableOneToManyCascadeDelete(ModelBuilder builder)
        {
            foreach (var relation in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relation.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
