﻿using FinalExam.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.EntityConfiguration
{
    class AnnouncementConfiguration : BaseEntityConfiguration<Announcement>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Announcement> builder)
        {
            base.ConfigureForeignKeys(builder);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Announcement> builder)
        {
            builder
                .Property(b => b.Title)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b=> b.Price)
                .HasDefaultValue(0.0M)
                .IsRequired();
            builder
                .Property(b => b.DatePublished)
                .IsRequired();


        }
    }
}
