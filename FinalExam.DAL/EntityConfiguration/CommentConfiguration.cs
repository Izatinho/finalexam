﻿using FinalExam.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalExam.DAL.EntityConfiguration
{
    public class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            base.ConfigureForeignKeys(builder);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            builder
                .Property(b => b.Content)
                .HasMaxLength(1000)
                .IsRequired();
            builder
                .Property(b => b.CreatedOn)
                .IsRequired();
        }
    }
}
