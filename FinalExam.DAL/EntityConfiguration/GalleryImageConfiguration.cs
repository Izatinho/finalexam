﻿using FinalExam.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.EntityConfiguration
{
    public class GalleryImageConfiguration : BaseEntityConfiguration<GalleryImage>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<GalleryImage> builder)
        {
            builder
                .HasOne(b => b.Announcement)
                .WithMany(b => b.GalleryImages)
                .HasForeignKey(b => b.AnnouncementId)
                .IsRequired();
        }
    
    }
}
