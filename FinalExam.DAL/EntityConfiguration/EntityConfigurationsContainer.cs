﻿using FinalExam.DAL.Entities;
using FinalExam.DAL.EntityConfiguration.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.EntityConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Announcement> AnnouncementConfiguration { get; }

        public IEntityConfiguration<Comment> CommentConfiguration { get; }
        public EntityConfigurationsContainer()
        {
            AnnouncementConfiguration = new AnnouncementConfiguration();
            CommentConfiguration = new CommentConfiguration();
        }

        
    }
}
