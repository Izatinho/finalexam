﻿using FinalExam.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.EntityConfiguration
{
    public class CategoryConfiguration : BaseEntityConfiguration<Category>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Category> builder)
        {
            
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Category> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .HasIndex(b => b.Id)
                .IsUnique();
        }
    }
}
