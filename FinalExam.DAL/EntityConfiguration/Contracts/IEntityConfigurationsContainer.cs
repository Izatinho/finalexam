﻿using FinalExam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.EntityConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Announcement> AnnouncementConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
    }
}
