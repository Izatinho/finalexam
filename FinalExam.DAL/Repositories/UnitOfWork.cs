﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Repositories.Contracts
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IAnnouncementRepository Announcements { get; }
        public ICommentRepository Comments { get; }
        public ICategoryRepository Categories { get; }
        public IGalleryImageRepository GalleryImages { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Announcements = new AnnouncementRepository(context);
            Comments = new CommentRepository(context);
            GalleryImages = new GalleryImageRepository(context);
            Categories = new CategoryRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
