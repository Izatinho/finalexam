﻿using FinalExam.DAL.Entities;
using FinalExam.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
