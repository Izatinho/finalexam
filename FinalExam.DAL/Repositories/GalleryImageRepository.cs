﻿using FinalExam.DAL.Entities;
using FinalExam.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Repositories
{
    public class GalleryImageRepository : Repository<GalleryImage>, IGalleryImageRepository
    {
        public GalleryImageRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.GalleryImages;
        }
    }
}
