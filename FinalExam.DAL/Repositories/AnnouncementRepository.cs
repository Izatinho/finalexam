﻿using FinalExam.DAL.Entities;
using FinalExam.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FinalExam.DAL.Repositories
{
    public class AnnouncementRepository : Repository<Announcement>, IAnnouncementRepository
    {
        public AnnouncementRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Announcements;
        }

        public IEnumerable<Announcement> GetAllWithAuthorsAndComments()
        {
            return entities
                .Include(e => e.Author)
                .Include(e => e.Comments)
                .ThenInclude(c => c.Author)
                .ToList();
        }

        public Announcement GetByIdWithAuthorsAndComments(int id)
        {
            return entities
                .Include(e => e.Author)
                .Include(e => e.Comments)
                .ThenInclude(c => c.Author)
                .FirstOrDefault(c => c.Id == id);
        }
    }
}
