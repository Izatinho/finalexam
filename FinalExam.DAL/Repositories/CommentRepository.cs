﻿using FinalExam.DAL.Entities;
using FinalExam.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }
    }
}
