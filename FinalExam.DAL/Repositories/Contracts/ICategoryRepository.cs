﻿using FinalExam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
