﻿using FinalExam.DAL.Entities;
using System.Collections.Generic;

namespace FinalExam.DAL.Repositories.Contracts
{
    public interface IAnnouncementRepository : IRepository<Announcement>
    {
        IEnumerable<Announcement> GetAllWithAuthorsAndComments();

        Announcement GetByIdWithAuthorsAndComments(int id);
    }
}