﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalExam.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
       UnitOfWork Create();
    }
}
